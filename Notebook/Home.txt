Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-08-10T22:13:45-04:00

====== Home ======
Welcome to the documentation for Senbir!  If you're here, chances are, you're curious about the TC-06 processor lurking in your virtual computer.  So, if I may redirect you: head over to [[CPU info]] to get started learning about the OP-codes supported by the TC-06 assembler!

===== ABOUT SENBIR =====
Senbir is a pretty implementation of the (fictional!) TC-06 processor architecture in Unity's C#.  It's a "game" insofar as it gives a goal to you, as well as some amount of control, and a win condition (the lose condition is if you crash your in-game computer somehow :p) - beyond that, it's basically just a way to render/handle I/O for a virtual machine.  Senbir itself was originally made in 48 hours for Ludum Dare 42, though the TC-06 architecture was in development for a few days before that.

Your goal in the game (at first, at least) is simply to write a BIOS/bootloader of sorts that can load & run the win-screen program from your hard-drive.

===== DIFFICULTIES/GAMEMODES =====
Senbir includes a few different gamemodes/difficulties for you to play in.

NOTE: Consistently, through these modes, some data ports will remain the same.
0. Port 0 is always going to be your monitor.
1. Port 1 is always going to be your first/main harddrive.
2. Port 2 is always going to be your keyboard.  It doesn't do anything.  Yet.
3. Port 3 is always going to be your mouse.  It doesn't do anything.  Yet.
4. Ports 4 through 6 will sometimes be other harddrives.
5. Ports 7 and onwards are currently undefined.  This may change in later releases.

==== Ludum Dare ====
The Ludum Dare difficulty is the original, designed to really fall in line with the original Ludum Dare 42 theme - "running out of space".
Your computer has 128 bytes of RAM (32 memory addresses), your drive has 1kB of storage (256 addresses), your monitor is 16x8 with 4 colors (128 VRam addresses/512 bytes, colors are black, white, dark lime, and light lime), and your clock speed is 60 Hz.

In this mode, the default state of the harddrive is a "WIN!" screen renderer, with address zero containing the number of addresses that should be loaded into RAM from address 0 to run the program.

==== Extended Mode ====
This mode is more sandboxy, but still has fairly tight constraints.  It's less painful than the Ludum Dare theme, though!
Your computer has 1kB of RAM (256 memory addresses), your drive has 8kB of storage (2048 addresses), your monitor is still 16x8 with 4 colors (128 VRam addresses/512 bytes, colors are black, white, dark lime, and light lime), and your clock speed is still 60 Hz.

==== Custom Mode ====
This is the ultimate sandbox mode, letting you pick ANY values for the following computer stats:
* RAM capability, up to 16,777,216 addresses/64MB - due to the 24-bit address limitation in MOVI/MOVO
* Drive storage space, up to 4,294,967,296 addresses/16GB - don't go this high!!  Your IRL computer will explode!!
* Number of drives, up to 4, taking up port 1 for the first drive, then 4-6 for the extra 3
* The monitor 's bittage (how many bits are dedicated to X, the Y bittage is equal to X-1) & color capabilities (how many bits are dedicated to color?  can be up to 32-X-Y)
* Clock speed - though don't expect Unity to be willing to run it much higher THAN 60 Hz - it has enough trouble with that as-is!
It also lets you put in a preset name, letting you load said preset, and have as many instances of a given computer build as you want (for the purposes of harddrives), meaning that it's a great mode if you want to actually toy around with building larger programs, like videogames, or even custom operating systems.

==== Tutorial Modes ====
In the Tutorials, you're given interactive & specific tasks to complete, in order to introduce you to the game, the mechanics of the TC-06, etc.  It operates under the Ludum Dare rules, though the monitor is black/white/dark orange/light orange, and the contents of the drive may be different, depending on the tutorial.

===== VARIOUS NOTES =====
If you can build your very own OS in Senbir, contact me at my [[mailto:clifracerx@protonmail.com|email]] because **HOLY SHIT** I need to see this.

To use the keyboard, you need to look at it, and press your Interact key (F by default) - please note that this prevents further interactions until you "Free" your keyboard with the Free key(s) (ESCAPE or RIGHT CONTROL).  The mouse is the same way, and will remain captured until Free'd.  That said, you can use the Look key (left alt) to look around freely.
