﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PortScreen : DataPort
{
	// The Texture2D used to actually render the screen.  Applied to a Material.
	public Texture2D screenTex;
	// A screen mesh.
	public MeshRenderer screenMesh;
	// The width & height of this screen, in pixels.
	public int width,height;
	// The list of colors this screen supports.  Make sure it's a power of two.  E.G, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, ...
	public Color[] myColors;
	// How many bits are dedicated to the color in SetData?
	public int colorBittage,colorMask;
	// How many bits are dedicated to the X and Y positions?
	public int xBittage,yBittage,xMask,yMask;

	public int[,] colSets,commands;

	void Start()
	{
		//if(GameManager.instance != null)
		{
			myColors = GameManager.myColors;
			colorBittage = GameManager.colorBittage;
			colorMask = GameManager.colorMask;
			xBittage = GameManager.xBittage;
			yBittage = GameManager.yBittage;
			xMask = GameManager.xMask;
			yMask = GameManager.yMask;
			width = GameManager.width;
			height = GameManager.height;
		}
		screenTex = new Texture2D(width, height);
		screenTex.filterMode = FilterMode.Point;
		screenMesh.material.mainTexture = screenTex;
		screenMesh.material.SetTexture("_EmissionMap", screenTex);
		for(int x = 0; x<width; x++)
		{
			for(int y = 0; y<height; y++)
			{
				screenTex.SetPixel(x, y, myColors[0]);
			}
		}
		screenTex.Apply();
		colSets = new int[width, height];
		commands = new int[width, height];
	}

	public override void PowerOff()
	{
		for(int x = 0; x<width; x++)
		{
			for(int y = 0; y<height; y++)
			{
				screenTex.SetPixel(x, y, myColors[0]);
			}
		}
		screenTex.Apply();
	}

	public override int GetData(int data) //XB x-pos, YB y-pos, returns the EXACT DATA that was passed in with SetData
	{
		int dataX = Convert.ToInt32(Convert.ToString(data, 2).PadLeft(32, '0'), 2);
		int x = (dataX >> (32-xBittage)) & xMask;
		int y = (dataX >> (32-yBittage-xBittage)) & yMask;
		return commands[x, y];
	}

	//NOTES: a 1920x1080 screen needs 11 bits for X, 11 bits for Y, leaving 6 bits of color, or up to 64 colors.  :c
	//A 1366x768 one needs 11 for X, 10 for Y, giving 128 colors.
	//Minecraft's resolution, 854x480 gives 10 bits for X, 9 for Y, giving 512 colors.
	//Trying to get real-color pixels on a large monitor WILL be impossible.  Looking at 25 bits of color.  Wooph.

	public override void SetData(int data) //NB color, XB x-pos, YB y-pos
	{
		int dataX = Convert.ToInt32(Convert.ToString(data, 2).PadLeft(32, '0'), 2);
		int color = (dataX >> (32-colorBittage)) & colorMask;
		int x = (dataX >> (32-xBittage-colorBittage)) & xMask;
		int y = (dataX >> (32-yBittage-xBittage-colorBittage)) & yMask;
		//Debug.Log("DATA: "+dataX+", COLOR: "+color+", X: "+x+", Y: "+y);
		screenTex.SetPixel(x, height-y-1, myColors[color]);
		commands[x, y] = data;
		colSets[x, y] = color;
		screenTex.Apply();
	}

	public override void ExtendedSetData(int data1, int data2) //NB color, XB x-pos, YB y-pos
	{
		Debug.LogWarning("WARNING: ExtendedSetData is not yet implemented for PortScreen");
		int dataX = Convert.ToInt32(Convert.ToString(data1, 2).PadLeft(32, '0'), 2);
		int color = (dataX >> (32-colorBittage)) & colorMask;
		int x = (dataX >> (32-xBittage-colorBittage)) & xMask;
		int y = (dataX >> (32-yBittage-xBittage-colorBittage)) & yMask;
		dataX = Convert.ToInt32(Convert.ToString(data2, 2).PadLeft(32, '0'), 2);
		x += (dataX >> (32-xBittage)) & xMask;
		y += (dataX >> (32-yBittage-xBittage)) & yMask;
	}
}