﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortKeyboard : DataPort
{
	public List<int> lastChars;
	public bool[] keyState;
	public float repeatSpeed,repeatSpeedMax;

	void Update()
	{
		repeatSpeed -= Time.deltaTime;
		if(repeatSpeed <= 0)
		{
			repeatSpeed = repeatSpeedMax;
		}
	}

	public override int GetData(int data) //32B address
	{
		int r = 0;
		int flag = (data >> 30) & 0x3;
		if(flag == 0)
		{
			if(lastChars.Count > 0)
			{
				r = lastChars[0];
				lastChars.RemoveAt(0);
			}
		}
		else if(flag==1)
		{
			int reqKey = (data >> 22) & 0xF;
			r = keyState[reqKey] ? 1 : 0;
		}
		return r; //DO NOTHING
	}

	public override void SetData(int data) //24B DATA - SET POINTER
	{
		for(int i = 0; i < keyState.Length; i++)
		{
			keyState[i] = false;
		}
		lastChars.Clear();
	}

	public override void ExtendedSetData(int data1, int data2) //32B DATA - EXPECTS A FULL 32 BITS OF INPUT
	{
		SetData(data1);
	}

	public override void PowerOff()
	{
		for(int i = 0; i < keyState.Length; i++)
		{
			keyState[i] = false;
		}
		lastChars.Clear();
	}
}