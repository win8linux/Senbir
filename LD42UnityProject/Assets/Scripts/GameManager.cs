﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

[System.Serializable]
public class CompStatSet
{
	public string save;
	public int ram,cyclesPerSec;
	public int width,height,colorBittage,colorMask,xBittage,yBittage,xMask,yMask;
	public Color[] myColors;
	public int[] driveStorage;
	public int tLength;
}

public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	public string preset = "DefaultCustom";
	public static string save;
	public static int ram,cyclesPerSec;
	public static int width,height,colorBittage,colorMask,xBittage,yBittage,xMask,yMask;
	public static Color[] myColors;
	public static int[] driveStorage;
	public int[] driveDefault,driveExtended;
	public int tLength = 0;
	public enum MenuScreen {MainMenu = 0, Options = 1, Tutorials = 2}
	public MenuScreen myState;
	public int tutorials;
	public int maxTutorials;
	public static int tutorialID = -1;
	public string[] tutorialNames;
	[Multiline(10)]
	public string[] tutorialDescriptions;
	public CompStatSet[] tutorialStats;

	void Start()
	{
		instance = this;
		tutorialID = -1;
		ram = 32;
		cyclesPerSec = 60;
		driveStorage = new int[256];
		tLength = 256;
		myColors = new Color[4];
		myColors[0] = Color.black;
		myColors[1] = Color.white;
		myColors[2] = Color.blue;
		myColors[3] = Color.cyan;
		width = 16;
		height = 8;
		colorBittage = 2;
		xBittage = 4;
		yBittage = 3;
		colorMask = 3;
		xMask = 15;
		yMask = 7;
		if(PlayerPrefs.HasKey("CustomLast"))
		{
			save = PlayerPrefs.GetString("CustomLast");
		}
		if(PlayerPrefs.HasKey("TutorialsFinished"))
		{
			tutorials = PlayerPrefs.GetInt("TutorialsFinished");
		}
		LoadPreset();
	}

	public static void SetTutorialsFinished(bool preserve, int number)
	{
		if(preserve)
		{
			int Ttutorials = PlayerPrefs.GetInt("TutorialsFinished");
			if(Ttutorials <= number)
			{
				PlayerPrefs.SetInt("TutorialsFinished", number);
			}
		}
		else
		{
			PlayerPrefs.SetInt("TutorialsFinished", number);
		}
	}

	//[System.Serializable]
	public GUIStyle style;
	//[System.Serializable]
	public GUIStyle styleLabel;

	void OnGUI()
	{
		if(myState == MenuScreen.MainMenu)
		{
			if(tutorials >= maxTutorials)
			{
				if(GUI.Button(new Rect((Screen.width / 2) - 128, (Screen.height / 2) - 64 - 128, 256, 128), "PLAY: DEFAULT", style))
				{
					ram = 32;
					cyclesPerSec = 60;
					width = 16;
					height = 8;
					colorBittage = 2;
					colorMask = 3;
					xBittage = 4;
					yBittage = 3;
					xMask = 15;
					yMask = 7;
					myColors = new Color[4];
					myColors[0] = Color.black;
					myColors[1] = Color.white;
					myColors[2] = new Color(0.407843137f, 0.662745098f, 0.152941176f, 1.0f);
					myColors[3] = new Color(0.678431373f, 0.941176471f, 0.415686275f, 1.0f);
					driveStorage = driveDefault;
					if(File.Exists(Application.dataPath + "/" + save + "/" + "DiskImage.cdmgpt"))
					{
						StreamReader sr = new StreamReader(Application.dataPath + "/" + save + "/" + "DiskImage.cdmgpt");
						string thing = sr.ReadToEnd();
						string[] lines = thing.Split('\n');
						for(int i = 0; i < lines.Length; i++)
						{
							driveStorage[i] = Convert.ToInt32(lines[i], 2);
						}
						sr.Close();
					}
					UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/SampleScene");
				}
				if(GUI.Button(new Rect((Screen.width / 2) - 128, (Screen.height / 2) - 64, 256, 128), "PLAY: EXTENDED", style))
				{
					ram = 256;
					cyclesPerSec = 60;
					width = 16;
					height = 8;
					colorBittage = 2;
					colorMask = 3;
					xBittage = 4;
					yBittage = 3;
					xMask = 15;
					yMask = 7;
					myColors = new Color[4];
					myColors[0] = Color.black;
					myColors[1] = Color.white;
					myColors[2] = new Color(0.407843137f, 0.662745098f, 0.152941176f, 1.0f);
					myColors[3] = new Color(0.678431373f, 0.941176471f, 0.415686275f, 1.0f);
					driveStorage = driveExtended;
					if(File.Exists(Application.dataPath + "/" + save + "/" + "DiskImage.cdmgpt"))
					{
						StreamReader sr = new StreamReader(Application.dataPath + "/" + save + "/" + "DiskImage.cdmgpt");
						string thing = sr.ReadToEnd();
						string[] lines = thing.Split('\n');
						for(int i = 0; i < lines.Length; i++)
						{
							driveStorage[i] = Convert.ToInt32(lines[i], 2);
						}
						sr.Close();
					}
					UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/SampleScene");
				}
				if(GUI.Button(new Rect((Screen.width / 2) - 128, (Screen.height / 2) - 64 + 128, 256, 128), "PLAY: CUSTOM", style))
				{
					myState = MenuScreen.Options;
				}
				if(GUI.Button(new Rect((Screen.width / 2) - 64, 32, 128, 64), "PLAY: TUTORIALS", style))
				{
					myState = MenuScreen.Tutorials;
				}
				if(GUI.Button(new Rect((Screen.width / 2) - 64, (Screen.height) - 32 - 64, 128, 64), "QUIT", style))
				{
					Application.Quit();
				}
			}
			else
			{
				if(GUI.Button(new Rect((Screen.width / 2) - 64, 32, 128, 64), "PLAY!", style))
				{
					myState = MenuScreen.Tutorials;
				}
				if(GUI.Button(new Rect((Screen.width / 2) - 64, (Screen.height) - 32 - 64, 128, 64), "QUIT", style))
				{
					Application.Quit();
				}
			}
		}
		else if(myState == MenuScreen.Options)
		{
			int x = 0;
			GUI.Label(new Rect((Screen.width / 2) - 128, 32, 192, 24), "RAM addresses (<=16,777,216)", styleLabel);
			ram = int.Parse(GUI.TextField(new Rect((Screen.width / 2) - 128, 48+x, 192, 32), "" + ram, style));
			GUI.Label(new Rect((Screen.width / 2) + 64, 32, 64, 24), "Cycle/S", styleLabel);
			cyclesPerSec = int.Parse(GUI.TextField(new Rect((Screen.width / 2) + 64, 48+x, 64, 32), "" + cyclesPerSec, style));
			//x += 8;
			GUI.Label(new Rect((Screen.width / 2) - 128, 48 + 32+x, 128, 24), "Monitor Color bits", styleLabel);
			colorBittage = int.Parse(GUI.TextField(new Rect((Screen.width / 2) - 128, 48 + 32+x+24, 128, 32), "" + colorBittage, style));
			GUI.Label(new Rect((Screen.width / 2), 48 + 32+x, 128, 24), "X-bits (Y=X-1)", styleLabel);
			xBittage = int.Parse(GUI.TextField(new Rect((Screen.width / 2), 48 + 32+x+24, 128, 32), "" + xBittage, style));
			yBittage = xBittage - 1;
			colorMask = ((int)Mathf.Pow(2, colorBittage)) - 1;
			xMask = ((int)Mathf.Pow(2, xBittage)) - 1;
			yMask = ((int)Mathf.Pow(2, yBittage)) - 1;
			width = xMask + 1;
			height = yMask + 1;
			if(myColors.Length != (Mathf.Pow(2, colorBittage)))
			{
				myColors = new Color[(int)Mathf.Pow(2, colorBittage)];
				for(int c = 0; c < myColors.Length; c++)
				{
					myColors[c].a = 1;
				}
			}
			x += 8;
			GUI.Label(new Rect((Screen.width / 2) - 128, 128+8, 256, 24), "LIST OF MONITOR COLORS", styleLabel);
			scrollCol = GUI.BeginScrollView(new Rect((Screen.width / 2) - 128, 128 + 32, 256, 128), scrollCol, new Rect(0, 0, 256, 32 * myColors.Length), style, style);
			GUI.backgroundColor= Color.white;
			for(int i = 0; i < myColors.Length; i++)
			{
				GUI.backgroundColor = myColors[i];
				if(GUI.Button(new Rect(0, 32 * i, 256, 32), "SET COLOR: " + i, style))
				{
					colorWindowUp = true;
					colorNum = i;
				}
			}
			GUI.backgroundColor = Color.white;
			GUI.EndScrollView();
			loadDrive = GUI.Toggle(new Rect((Screen.width / 2) - 128, -256 + 512 + 64, 32, 32), loadDrive, "USE", style);
			driveString = GUI.TextField(new Rect((Screen.width / 2) - 128 + 32, -256 + 512 + 64, 256 - 32, 32), driveString, style);
			if(!loadDrive)
			{
				GUI.Label(new Rect((Screen.width / 2) - 128, -256 + 512 + 64 + 32, 256, 24), "Drive length in addresses (<=4,294,967,296)", styleLabel);
				tLength = int.Parse(GUI.TextField(new Rect((Screen.width / 2) - 128, -256 + 512 + 64 + 48+8, 256, 32), "" + tLength, style));
			}
			bool canPlay = true;
			if(colorBittage + xBittage + yBittage > 32 || ram > 16777216 || colorBittage < 1 || xBittage < 1 || yBittage < 1)
			{
				canPlay = false;
			}
			string playText = canPlay ? "PLAY!" : "ERROR IN VARIABLES?";
			if(GUI.Button(new Rect((Screen.width / 2) - 64, (Screen.height) - 256, 128, 32), playText, style) && canPlay)
			{
				if(loadDrive)
				{
					StreamReader sr = new StreamReader(Application.dataPath + "/" + save + "/" + driveString + ".cdmgpt");
					string thing = sr.ReadToEnd();
					string[] lines = thing.Split('\n');
					for(int i = 0; i < lines.Length; i++)
					{
						driveStorage[i] = Convert.ToInt32(lines[i], 2);
					}
					sr.Close();
				}
				else
				{
					driveStorage = new int[tLength];
				}
				UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/SampleScene");
				SavePreset();
			}
			save = GUI.TextField(new Rect((Screen.width / 2) - 128, Screen.height - 128 - 64, 256, 32), save, style);
			if(GUI.Button(new Rect((Screen.width / 2) - 128, (Screen.height) - 128, 128, 64), "SAVE PRESET?", style))
			{
				SavePreset();
			}
			if(GUI.Button(new Rect((Screen.width / 2), (Screen.height) - 128, 128, 64), "Load PRESET?", style))
			{
				LoadPreset();
			}
			if(GUI.Button(new Rect((Screen.width / 2) - 64, (Screen.height) - 64, 128, 64), "BACK", style))
			{
				myState = MenuScreen.MainMenu;
			}
		}
		else if(myState == MenuScreen.Tutorials)
		{
			int t = tutorials + 1;
			if(t > tutorialNames.Length)
			{
				t = tutorialNames.Length;
			}
			GUI.Label(new Rect((Screen.width / 2) - 128, 16, 256, 24), "LEVEL LIST", styleLabel);
			scrollTut = GUI.BeginScrollView(new Rect((Screen.width / 2) - 128, 48, 256, 128), scrollTut, new Rect(0, 0, 256, 32 * t), style, style);
			for(int i = 0; i < t; i++)
			{
				if(GUI.Button(new Rect(0, 32 * i, 256, 32), tutorialNames[i], style))
				{
					selectedTut = i;
				}
			}
			GUI.EndScrollView();
			GUI.TextArea(new Rect((Screen.width / 2) - 128, (48+128), 256, 128), tutorialDescriptions[selectedTut], style);
			if(GUI.Button(new Rect((Screen.width / 2) - 64, (Screen.height) - 96, 128, 32), "PLAY!", style))
			{
				tutorialID = selectedTut;
				CompStatSet css = tutorialStats[selectedTut];
				save = css.save;
				ram = css.ram;
				cyclesPerSec = css.cyclesPerSec;
				colorBittage = css.colorBittage;
				colorMask = css.colorMask;
				xBittage = css.xBittage;
				xMask = css.xMask;
				yBittage = css.yBittage;
				yMask = css.yMask;
				myColors = css.myColors;
				driveStorage = css.driveStorage;
				UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/SampleScene");
			}
			if(GUI.Button(new Rect((Screen.width / 2) - 64, (Screen.height) - 64, 128, 64), "BACK", style))
			{
				myState = MenuScreen.MainMenu;
			}
		}
		if(colorWindowUp)
		{
			colorWinRect = GUI.Window(1, colorWinRect, ColorWindow, "Set Color", style);
		}
	}

	public Vector2 scrollTut;
	public int selectedTut;

	public Rect colorWinRect;
	public Vector2 colorWScroll;

	void ColorWindow(int id)
	{
		colorWScroll = GUILayout.BeginScrollView(colorWScroll, style);
		GUILayout.Label("Red: 0-255");
		myColors[colorNum].r = int.Parse(GUILayout.TextField("" + (255 * myColors[colorNum].r), style)) / 255f;
		GUILayout.Label("Green: 0-255");
		myColors[colorNum].g = int.Parse(GUILayout.TextField("" + (255 * myColors[colorNum].g), style)) / 255f;
		GUILayout.Label("Blue: 0-255");
		myColors[colorNum].b = int.Parse(GUILayout.TextField("" + (255 * myColors[colorNum].b), style)) / 255f;
		GUI.backgroundColor = myColors[colorNum];
		if(GUILayout.Button("Close Window, COL#"+colorNum, style))
		{
			colorWindowUp = false;
		}
		GUI.backgroundColor = Color.white;
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}

	void SavePreset()
	{
		if(!Directory.Exists(Application.dataPath + "/../" + save))
		{
			Directory.CreateDirectory(Application.dataPath + "/../" + save);
		}
		PlayerPrefs.SetString("CustomLast", save);
		PlayerPrefs.Save();
		StreamWriter sw = new StreamWriter(Application.dataPath+"/../"+save+"/"+"CustomPreset.cps");
		sw.WriteLine(ram);
		sw.WriteLine(cyclesPerSec);
		sw.WriteLine(colorBittage);
		sw.WriteLine(xBittage);
		for(int i = 0; i < myColors.Length; i++)
		{
			sw.WriteLine(myColors[i].r);
			sw.WriteLine(myColors[i].g);
			sw.WriteLine(myColors[i].b);
		}
		sw.WriteLine(driveString);
		sw.WriteLine(loadDrive);
		sw.WriteLine(tLength);
		sw.Close();
	}

	void LoadPreset()
	{
		PlayerPrefs.SetString("CustomLast", save);
		PlayerPrefs.Save();
		StreamReader sr = new StreamReader(Application.dataPath+"/../"+save+"/"+"CustomPreset.cps");
		ram = int.Parse(sr.ReadLine());
		cyclesPerSec = int.Parse(sr.ReadLine());
		colorBittage = int.Parse(sr.ReadLine());
		xBittage = int.Parse(sr.ReadLine());
		myColors = new Color[(int)Mathf.Pow(2, colorBittage)];
		for(int i = 0; i < myColors.Length; i++)
		{
			myColors[i] = new Color(float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()), 1.0f);
			for(int c= 0; c<myColors.Length; c++)
			{
				myColors[c].a = 1;
			}
		}
		driveString = sr.ReadLine();
		loadDrive = bool.Parse(sr.ReadLine());
		tLength = int.Parse(sr.ReadLine());
		sr.Close();
	}

	public Vector2 scrollCol;
	public bool loadDrive = false;
	public string driveString;
	public bool colorWindowUp;
	public int colorNum;
}