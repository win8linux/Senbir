﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class Assembler : EditorWindow
{
	public string code;
	public string programName;
	Vector2 scroll;
	public int [,] monitorCopy;

	[MenuItem ("Window/C06Assembler")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(Assembler));
	}

	void OnGUI()
	{
		scroll = EditorGUILayout.BeginScrollView(scroll);
		GUILayout.Label("CODE INFO", EditorStyles.boldLabel);
		programName = EditorGUILayout.TextField("Name", programName);
		code = EditorGUILayout.TextArea(code);
		GUILayout.Label("SAVE/LOAD", EditorStyles.boldLabel);
		if(GUILayout.Button("Save"))
		{
			StreamWriter sw = new StreamWriter(Application.dataPath+"/../"+programName+".casm");
			sw.Write(code);
			sw.Close();
		}
		if(GUILayout.Button("Load"))
		{
			StreamReader sr = new StreamReader(Application.dataPath+"/../"+programName+".casm");
			code = sr.ReadToEnd();
			sr.Close();
		}
		GUILayout.Label("COMPILE", EditorStyles.boldLabel);
		GameObject selectedObject = UnityEditor.Selection.activeGameObject;
		if(GUILayout.Button("Compile"))
		{
			compiled = Compile();
		}
		if(selectedObject==null)
		{
			GUILayout.Label("NO SELECTED OBJECT!", EditorStyles.boldLabel);
		}
		else if(selectedObject.GetComponent<Computer>()!=null)
		{
			if(GUILayout.Button("Compile to ROM"))
			{
				selectedObject.GetComponent<Computer>().rom = compiled;
			}
		}
		else if(selectedObject.GetComponent<PortDrive>()!=null)
		{
			if(GUILayout.Button("Clear HDD"))
			{
				for(int i = 0; i<selectedObject.GetComponent<PortDrive>().driveStorage.Length; i++)
				{
					selectedObject.GetComponent<PortDrive>().driveStorage[i] = 0;
				}
			}
			if(compiled.Length>selectedObject.GetComponent<PortDrive>().driveStorage.Length)
			{
				GUILayout.Label("DRIVE TOO SMALL!  NEEDS "+(selectedObject.GetComponent<PortDrive>().driveStorage.Length-compiled.Length)+" MORE ADDRESSES OF STORAGE!", EditorStyles.boldLabel);
			}
			else
			{
				if(GUILayout.Button("Install"))
				{
					for(int i = 0; i<compiled.Length; i++)
					{
						selectedObject.GetComponent<PortDrive>().driveStorage[i]=compiled[i];
					}
				}
			}
		}
		else if(selectedObject.GetComponent<GameManager>()!=null)
		{
			if(GUILayout.Button("Install in Default slot"))
			{
				selectedObject.GetComponent<GameManager>().driveDefault = compiled;
			}
			if(GUILayout.Button("Install in Extended slot"))
			{
				selectedObject.GetComponent<GameManager>().driveExtended = compiled;
			}
			for(int i = 0; i < selectedObject.GetComponent<GameManager>().tutorialStats.Length; i++)
			{
				if(GUILayout.Button("Install in drive for: " + selectedObject.GetComponent<GameManager>().tutorialNames[i]))
				{
					selectedObject.GetComponent<GameManager>().tutorialStats[i].driveStorage = compiled;
				}
			}
		}
		else
		{
			GUILayout.Label("SELECTED OBJECT NOT DRIVE OR PROCESSOR W/ ROM!", EditorStyles.boldLabel);
		}
		EditorGUILayout.EndScrollView();
	}

	int[] compiled;

	// Compile the program into bytecode.
	int[] Compile()
	{
		List<int> compiled = new List<int>();
		string[] listLines = code.Split('\n');
		foreach(string s in listLines)
		{
			if(!s.StartsWith("//")) //ignore comments
			{
				//BitArray bits = new BitArray(32); // initialize the bits used to set up the memory address
				string bits = "";
				if(s.StartsWith("DATAC"))
				{
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==2)
					{
						bits+=s2[1];
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("NILLIST"))
				{
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==2)
					{
						bits+="00000000000000000000000000000000";
						int numParts = int.Parse(s2[1]);
						for(int i = 1; i<numParts; i++)
						{
							compiled.Add(0);
						}
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("NIL"))
				{
					bits+="00000000000000000000000000000000";
					//DO NOTHING, NIL IS 0x0000, TAKES NO ARGUMENTS, JUST WASTES A CLOCK CYCLE
				}
				else if(s.StartsWith("HLT")) // HLT [ticks]
				{
					bits+="0001";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==2)
					{
						string converted = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted.Length<28)
						{
							converted = converted.PadLeft(28, '0');
						}
						if(converted.Length!=28)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  HALT TICK COUNT MUST BE LESS THAN OR EQUAL TO 28 BITS!  "+converted);
						}
						bits+=converted;
					}
					else if(s2.Length==1)
					{
						bits += "".PadLeft(28, '0');
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("MOVI"))
				{
					bits+="0010";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==3)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  REGISTER MUST BE LESS THAN OR EQUAL TO 2 BITS!"+converted1);
						}
						bits+=converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length<24)
						{
							converted2 = converted2.PadLeft(24, '0');
						}
						if(converted2.Length!=24)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ADDRESS MUST BE LESS THAN OR EQUAL TO 12 BITS!"+converted2);
						}
						bits+=converted2;
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("MOVO"))
				{
					bits+="0011";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==3)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  REGISTER MUST BE LESS THAN OR EQUAL TO 2 BITS!"+converted1);
						}
						bits+=converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length<24)
						{
							converted2 = converted2.PadLeft(24, '0');
						}
						if(converted2.Length!=24)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ADDRESS MUST BE LESS THAN OR EQUAL TO 12 BITS!"+converted2);
						}
						bits+=converted2;
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("JMP"))
				{
					bits+="0100";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==3)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length<2)
						{
							converted1 = converted1.PadLeft(2, '0');
						}
						if(converted1.Length!=2)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  FLAG MUST BE LESS THAN OR EQUAL TO 2 BITS!"+converted1);
						}
						bits+=converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length<24)
						{
							converted2 = converted2.PadLeft(24, '0');
						}
						if(converted2.Length!=24)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  DESTINATION MUST BE LESS THAN OR EQUAL TO 26 BITS!"+converted2);
						}
						bits+=converted2;
						bits+="00";
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("SETDATA"))
				{
					bits+="0101";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==4)
					{
						string converted = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted.Length<4)
						{
							converted = converted.PadLeft(4, '0');
						}
						if(converted.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  PORT MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted);
						}
						bits+=converted;
						converted = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted.Length<2)
						{
							converted = converted.PadLeft(2, '0');
						}
						if(converted.Length!=2)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  FLAG MUST BE LESS THAN OR EQUAL TO 2 BITS!"+converted);
						}
						bits+=converted;
						int flag = int.Parse(s2[2]);
						if(flag == 0)
						{
							bits+=s2[3];
						}
						else if(flag == 1 || flag == 2)
						{
							int addr = int.Parse(s2[3]);
							bool neg = false;
							if(addr<0)
							{
								addr = -addr;
								neg = true;
							}
							if(neg)
								bits += "0";
							else
								bits += "1";
							bits += Convert.ToString(addr, 2).PadLeft(21, '0');
						}
						else if(flag == 3)
						{
							int reg = int.Parse(s2[3]);
							bits += Convert.ToString(reg, 2).PadLeft(4, '0').PadRight(22, '0');
						}
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("GETDATA"))
				{
					bits+="0110";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==4)
					{
						string converted = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted.Length<4)
						{
							converted = converted.PadLeft(4, '0');
						}
						if(converted.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  PORT MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted);
						}
						bits+=converted;
						converted = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted.Length<2)
						{
							converted = converted.PadLeft(2, '0');
						}
						if(converted.Length!=2)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  FLAG MUST BE LESS THAN OR EQUAL TO 2 BITS!"+converted);
						}
						bits+=converted;
						int flag = int.Parse(s2[2]);
						if(flag == 0)
						{
							bits+=s2[3];
						}
						else if(flag == 1 || flag == 2)
						{
							int addr = int.Parse(s2[3]);
							bool neg = false;
							if(addr<0)
							{
								addr = -addr;
								neg = true;
							}
							if(neg)
								bits += "0";
							else
								bits += "1";
							bits += Convert.ToString(addr, 2).PadLeft(21, '0');
						}
						else if(flag == 3)
						{
							int reg = int.Parse(s2[3]);
							bits += Convert.ToString(reg, 2).PadLeft(4, '0').PadRight(22, '0');
						}
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("SET"))
				{
					bits+="0111";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==4)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted1);
						}
						bits+=converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length<2)
						{
							converted2 = converted2.PadLeft(2, '0');
						}
						if(converted2.Length!=2)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  BYTE POSITION MUST BE LESS THAN OR EQUAL TO 2 BITS!"+converted2);
						}
						bits+=converted2;
						bits+=s2[3];
						bits = bits.PadRight(32, '0');
						if(bits.Length < 32)
						{
							bits += "0";
						}
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("IFJMP"))
				{
					bits+="1000";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==4)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length<2)
						{
							converted1 = converted1.PadLeft(2, '0');
						}
						if(converted1.Length!=2)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  FLAG MUST BE LESS THAN OR EQUAL TO 2 BITS!"+converted1);
						}
						bits+=converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length<24)
						{
							converted2 = converted2.PadLeft(24, '0');
						}
						if(converted2.Length!=24)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ADDRESS MUST BE LESS THAN OR EQUAL TO 24 BITS!"+converted2);
						}
						bits+=converted2;
						//bits+=s2[3];
						converted2 = Convert.ToString(int.Parse(s2[3]), 2);
						if(converted2.Length<2)
						{
							converted2 = converted2.PadLeft(2, '0');
						}
						if(converted2.Length!=2)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  OPERATION MUST BE LESS THAN OR EQUAL TO 12 BITS!"+converted2);
						}
						bits+=converted2;
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("PMOV"))
				{
					bits+="1001";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==7)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  SOURCE REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted1);
						}
						bits+=converted1;
						converted1 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  TARGET REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted1);
						}
						bits+=converted1;
						converted1 = Convert.ToString(int.Parse(s2[3]), 2);
						if(converted1.Length<5)
						{
							converted1 = converted1.PadLeft(5, '0');
						}
						if(converted1.Length!=5)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  START MUST BE LESS THAN OR EQUAL TO 5 BITS!"+converted1);
						}
						bits+=converted1;
						converted1 = Convert.ToString(int.Parse(s2[4]), 2);
						if(converted1.Length<5)
						{
							converted1 = converted1.PadLeft(5, '0');
						}
						if(converted1.Length!=5)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  END MUST BE LESS THAN OR EQUAL TO 5 BITS!"+converted1);
						}
						bits+=converted1;
						string converted2 = Convert.ToString(int.Parse(s2[5]), 2);
						if(converted2.Length<5)
						{
							converted2 = converted2.PadLeft(5, '0');
						}
						if(converted2.Length!=5)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  SHIFT MUST BE LESS THAN OR EQUAL TO 5 BITS!"+converted2);
						}
						bits+=converted2;
						bits+=s2[6]; //1 or 0
						bits+="0000";
						if(bits.Length!=32)
							throw new Exception("WTF WHY IS IT SHORT: "+bits);
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("MATH"))
				{
					bits+="1010";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count-remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length==5)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  SOURCE REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted1);
						}
						bits+=converted1;
						converted1 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  TARGET REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted1);
						}
						bits+=converted1;
						string converted2 = Convert.ToString(int.Parse(s2[3]), 2);
						if(converted2.Length<4)
						{
							converted2 = converted2.PadLeft(4, '0');
						}
						if(converted2.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  OPERATION MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted2);
						}
						bits+=converted2;
						bits+=s2[4];
					}
					else if(s2.Length==4)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  SOURCE REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted1);
						}
						bits+=converted1;
						converted1 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted1.Length<4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  TARGET REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted1);
						}
						bits+=converted1;
						string converted2 = Convert.ToString(int.Parse(s2[3]), 2);
						if(converted2.Length<4)
						{
							converted2 = converted2.PadLeft(4, '0');
						}
						if(converted2.Length!=4)
						{
							throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  OPERATION MUST BE LESS THAN OR EQUAL TO 4 BITS!"+converted2);
						}
						bits+=converted2;
						bits+="".PadLeft(16, '0');
					}
					else
					{
						throw new Exception("AT LINE "+s+" THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				Debug.Log(s+"\n"+bits);
				compiled.Add(Convert.ToInt32(bits, 2));
			}
		}
		return compiled.ToArray();
	}
}