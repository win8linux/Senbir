# Senbir: Extra Programs
This folder is dedicated to some of the programs built into Senbir, and that I wrote during testing in the main LD42 difficulty.

## BlinkenLightROM
Just a simple one-pixel flash in the upper left, designed to live in ROM.

## Blinkenlights
The same as BlinkenLightROM, but designed to be loaded with the included bootloader.

## Bootloader
The (admittedly outdated & a bit inefficient) bootloader program I wrote.

## DrawWin
The program loaded on the drive that draws the win screen.